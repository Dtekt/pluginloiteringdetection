#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//#include "plugininterface.h"  //demo

#include <QDir>
#include <QMainWindow>
#include <QList>
#include <QStack>
#include <QIcon>
#include <QThread>

#include "frameinfo.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Ui::MainWindow *ui;
    QStack<FrameInfo*> *database;
    
private slots:
//    void displayFrame(FrameInfo frameInfo, QImage image);  //QImage to be removed if worker thread can update UI
//    void on_actionEdit_Configuration_Profile_triggered();
//    void on_actionOpen_triggered();
//    void on_tabWidget_selected(const QString &arg1);
//    void on_pauseOrResumeBtn_toggled(bool checked);

private:
    bool loadPlugins();
    void updateLog(QString);

    QDir pluginsDir;
    QList<QObject> pluginList;
    QString videoFileName;
    QIcon playIcon;
    QIcon pauseIcon;

    QObject *singleplugindemo; // demo

    QThread processorThread;
};

#endif
