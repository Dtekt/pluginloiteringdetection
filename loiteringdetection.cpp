#include "loiteringdetection.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <QSound>

LoiteringDetection::LoiteringDetection()
{
    pluginName = "Loitering Detection";
    pluginDescription = "This plugin detects loitering pedestrians in the scene";
}

void LoiteringDetection::prepareGUI(MainWindow * win) const
{
    win->ui->menuPlugins->addAction(pluginName);
    win->ui->logView->appendPlainText(pluginName + " plugin loaded..\n" + QDateTime::currentDateTime().toString() + "\n");
                QSound::play("C:/Users/ANURUDDHA/C++/PluginLoiteringDetection/alarm/loitererAlarm.mp3");
}

void LoiteringDetection::detectAndDisplay(MainWindow *win, FrameInfo *frameInfo) const
{
    foreach (Pedestrian pedestrian, frameInfo->pedestriansVector) {
        if(2222 - pedestrian.timeAppeared > 23){
            QSound::play(":/loitererAlarm.mp3");
            win->ui->logView->appendPlainText("Loiterer Detected... :O");
        }
    }
}

Q_EXPORT_PLUGIN2(loiteringdetection,LoiteringDetection)
