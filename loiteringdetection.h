#ifndef LOITERINGDETECTION_H
#define LOITERINGDETECTION_H

#include <QObject>
#include "plugininterface.h"

class LoiteringDetection : public QObject, public PluginInterface
{
    Q_OBJECT
    Q_INTERFACES(PluginInterface)
public:
    explicit LoiteringDetection();
    void prepareGUI(MainWindow * win) const;
    void detectAndDisplay(MainWindow *win, FrameInfo *frameInfo) const;
};

#endif // LOITERINGDETECTION_H
